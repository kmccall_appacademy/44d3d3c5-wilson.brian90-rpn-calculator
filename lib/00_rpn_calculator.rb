class RPNCalculator
  attr_accessor :value
  @value = 0.0

  def initialize()
    @stack = []
  end

  def evaluate(string)
    tokens = tokens(string)
    while tokens.size > 1
      (0..tokens.length - 3).each do |idx|
        if tokens[idx + 2] == :+
          tokens[idx] += tokens[idx + 1].to_f
          2.times{tokens.delete_at(idx + 1)}
        elsif tokens[idx + 2] == :-
          tokens[idx] -= tokens[idx + 1].to_f
          2.times{tokens.delete_at(idx + 1)}
        elsif tokens[idx + 2] == :*
          tokens[idx] *= tokens[idx + 1].to_f
          2.times{tokens.delete_at(idx + 1)}
        elsif tokens[idx + 2] == :/
          tokens[idx] /= tokens[idx + 1].to_f
          2.times{tokens.delete_at(idx + 1)}
        end
      end
    end
    tokens[0]
  end

  def push(token)
    @stack.push(token)
  end

  def pop
    @stack.pop
  end

  def raise_error(error)
    raise StandardError.new(error)
  end

  def tokens(string)
    arith = "+-*/"
    nums = ("0".."9").to_a
    token_array = string.split(" ")
    token_array.map! do |t|
      if nums.include?(t)
        t = t.to_i
      elsif arith.include?(t)
        t = t.to_sym
      end
    end
    token_array
  end

  def plus
    if @stack.size < 1
      raise_error("calculator is empty")
    end

    if @stack.size == 1
      @value = @value + @stack.pop
      return @value
    end

    if @stack.size >= 2
      @value = (@stack.delete_at(-2) + @stack.pop)
      return @value
    end
  end

  def minus
    if @stack.size < 1
      raise_error("calculator is empty")
    end

    if @stack.size == 1
      @value = @value - @stack.pop
      return @value
    end
    if @stack.size >= 2
      @value = (@stack.delete_at(-2) - @stack.pop)
      return @value
    end
  end

  def divide
    if @stack.size < 1
      raise_error("calculator is empty")
    end

    if @stack.size == 1
      @value = (@value / @stack.pop.to_f).to_f
      return @value
    end
    if @stack.size >= 2
      @value = (@stack.delete_at(-2).to_f / @stack.pop.to_f)
      return @value
    end
  end

  def times
    if @stack.size < 1
      raise_error("calculator is empty")
    end

    if @stack.size == 1
      @value = (@value * @stack.pop.to_f)
      return @value
    end
    if @stack.size >= 2
      @value = (@stack.delete_at(-2).to_f * @stack.pop.to_f)
      return @value
    end
  end

end


calculator = RPNCalculator.new
